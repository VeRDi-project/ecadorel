ansible==2.8.6
git+https://gitlab.inria.fr/dpertin/madpp@packaging
click
kolla_ansible

pbr!=2.1.0,==2.0.0 # Apache-2.0

# ini parsing
oslo.config==5.2.0 # Apache-2.0

# YAML parsing
PyYAML==3.12 # MIT

# templating
Jinja2==2.8 # BSD License (3 clause)

# unit testing
docker==2.0.0 # Apache-2.0

