import argparse
import logging
import sys, os
import subprocess 

def setupLogging (args) :    
    logging.basicConfig(filename=args.log_file, level=logging.INFO)
    
    logging.Logger = logging.getLogger(__name__)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logging.Logger.addHandler(handler)



def createVagrantConfig (name, cpu, ram, os, prv, ip): 
    ret =       "  config.vm.define \"" + str (name) + "\"" + "do |" + str (name) + "|\n"
    ret = ret + "    " + str (name) + ".vm.box = \"" + str (os) + "\"\n"
    ret = ret + "    " + str (name) + ".vm.hostname = \"" + str (name) + "\"\n"
    ret = ret + "    " + str (name) + ".vm.network :private_network, ip: \"" + ip + "\"\n"
    ret = ret + "    " + str (name) + ".vm.provider \"" + prv + "\" do |v|\n"
    ret = ret + "      v.cpus = " + str (cpu) + "\n"
    ret = ret + "      v.memory = " + str (ram) + "\n"
    ret = ret + "    end\n"
    ret = ret + "  end\n"
    return ret

def launchVagrant ():
    cmd = ["vagrant", "up"]
    if subprocess.run (cmd).returncode:
        raise Exception ("vagrant failure")
        
def bootCluster (args) :
    vagrant = "Vagrant.configure (\"2\") do |config|\n"

    masters = {}
    computes = {}
    
    for i in range (0, args.master) : 
        vagrant = vagrant + createVagrantConfig ("m" + str (i), args.cpu_master, args.memory_master, args.os, args.provider, args.base_ip + str (i + 10))
        masters["m"+str(i)] = args.base_ip + str (i + 10)
        
    for i in range (0, args.compute) :
        vagrant = vagrant + createVagrantConfig ("c" + str (i), args.cpu_compute, args.memory_compute, args.os, args.provider, args.base_ip + str (i + args.master + 10))
        computes["m"+str(i)] = args.base_ip + str (i + 10 + args.master)
            
    vagrant = vagrant + "end"

    with open ("Vagrantfile", 'w') as stream:
        stream.write (vagrant)
        
    launchVagrant ()
    return (masters, computes)


def launchInstallUbuntu (name : str, ip : str, user: str, pword : str)-> None:
    key_path = "./.vagrant/machines/" + name + "/virtualbox/private_key"
    
    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "echo", "-e", "\"root\nroot\"", "|", "sudo", "passwd", "root"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "sudo", "cp", "/etc/ssh/sshd_config", "/etc/ssh/sshd_config2"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["scp", "-i", key_path, "./centos/sshd_config", "{0!s}@{1!s}:sshd_config".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "sudo", "mv", "sshd_config", "/etc/ssh/sshd_config"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "sudo", "service", "sshd", "restart"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    user = "root"
    pword = "root"
    
    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt", "update"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt", "install", "-y", "python3.8", "python3-pip", "emacs"]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "update-alternatives", "--install", "/usr/bin/python", "python", "/usr/bin/python3.8", "1"]
    r = subprocess.run (cmd).returncode
    #     raise Exception ("ln failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt-get", "install", "-y", "apt-transport-https", "ca-certificates", "curl", "gnupg-agent", "software-properties-common", "sshpass"]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "curl", "-fsSL", "https://download.docker.com/linux/ubuntu/gpg", "|", "sudo", "apt-key", "add", "-"]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "add-apt-repository",  "\"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\""]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt", "update"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")
    
    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt-get", "install", "-y", "docker-ce", "docker-ce-cli", "containerd.io"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "pip3", "install", "docker==2.4.2"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "pip3", "install", "oslo_config"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "/usr/bin/python", "--version"]
    if subprocess.run (cmd).returncode:
        raise Exception ("ln failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt-get", "install", "-y", "ansible"]
    if subprocess.run (cmd).returncode:
        raise Exception ("ln failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "groupadd", "docker"]
    ret = subprocess.run (cmd).returncode

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "usermod", "-aG", "docker", "vagrant"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "echo", "-e", "\"root\nroot\"", "|", "sudo", "passwd", "root"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "cp", "/etc/ssh/sshd_config", "/etc/ssh/sshd_config2"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "sh", "-c", "'echo \"PermitRootLogin yes\n\" >> /etc/ssh/sshd_config'"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "service", "sshd", "restart"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["sshpass", "-p", pword, "scp", "requirements.txt", "{0!s}@{1!s}:.".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("scp failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "pip3", "install", "-r", "requirements.txt"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["sshpass", "-p", pword, "scp", "kolla-hosts.ini", "{0!s}@{1!s}:.".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("scp failure")

    cmd = ["sshpass", "-p", pword, "scp", "kube-hosts.ini", "{0!s}@{1!s}:.".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("scp failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "systemctl", "enable", "systemd-timesyncd"]
    if subprocess.run (cmd).returncode:
        raise Exception ("time failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "systemctl", "start", "systemd-timesyncd"]
    if subprocess.run (cmd).returncode:
        raise Exception ("time failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "apt", "install", "-y", "mysql-server"]
    if subprocess.run (cmd).returncode:
        raise Exception ("time failure")
    
def configureClusterUbuntu (masters, computes):
    i = 0
    for m in masters :
        launchInstallUbuntu ("m" + str (i), masters [m], 'vagrant', 'vagrant')
        i = i + 1

    i = 0
    for c in computes:
        launchInstallUbuntu ("c" + str (i), computes [c], 'vagrant', 'vagrant');
        i = i + 1


def launchInstallCentos (name: str, ip : str, user: str, pword : str)-> None:
    key_path = "./.vagrant/machines/" + name + "/virtualbox/private_key"
    
    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "echo", "-e", "\"root\nroot\"", "|", "sudo", "passwd", "root"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "sudo", "cp", "/etc/ssh/sshd_config", "/etc/ssh/sshd_config2"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["scp", "-i", key_path, "./centos/sshd_config", "{0!s}@{1!s}:sshd_config".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "sudo", "mv", "sshd_config", "/etc/ssh/sshd_config"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["ssh", "{0!s}@{1!s}".format (user, ip), "-i", key_path, "sudo", "service", "sshd", "restart"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    user = "root"
    pword = "root"
    
    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "who"]
    if subprocess.run (cmd).returncode:
        raise Exception ("ln failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "install", "-y", "epel-release"]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")
    
    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "makecache"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "install", "-y", "python3.8", "python3-pip", "emacs", "yum-utils", "sshpass", "git", "libselinux-python3", "redhat-rpm-config"]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum-config-manager", "--add-repo", "https://download.docker.com/linux/centos/docker-ce.repo"]
    if subprocess.run (cmd).returncode:
        raise Exception ("install failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "makecache"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")
    
    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "install", "-y", "docker-ce", "docker-ce-cli", "containerd.io"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "pip3", "install", "docker==2.4.2"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "pip3", "install", "oslo_config"]
    if subprocess.run (cmd).returncode:
        raise Exception ("Update failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "install", "-y", "ansible"]
    if subprocess.run (cmd).returncode:
        raise Exception ("ln failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "groupadd", "docker"]
    ret = subprocess.run (cmd).returncode

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "usermod", "-aG", "docker", "vagrant"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "service", "docker", "start"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")

    cmd = ["sshpass", "-p", pword, "scp", "requirements.txt", "{0!s}@{1!s}:.".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("scp failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "pip3", "install", "-r", "requirements.txt"]
    if subprocess.run (cmd).returncode:
        raise Exception ("docker failure")
    
    cmd = ["sshpass", "-p", pword, "scp", "kolla-hosts.ini", "{0!s}@{1!s}:.".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("scp failure")

    cmd = ["sshpass", "-p", pword, "scp", "kube-hosts.ini", "{0!s}@{1!s}:.".format (user, ip)]
    if subprocess.run (cmd).returncode:
        raise Exception ("scp failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "yum", "install", "-y", "mysql-server"]
    if subprocess.run (cmd).returncode:
        raise Exception ("time failure")

    cmd = ["sshpass", "-p", pword, "ssh", "{0!s}@{1!s}".format (user, ip), "sudo", "ln", "-sf", "/usr/bin/python3", "/usr/bin/python"] # alternatives does not work on centos/8
    if subprocess.run (cmd).returncode:
        raise Exception ("time failure")
        
def configureClusterCentos (masters, computes) :
    i = 0
    for m in masters :
        launchInstallCentos ("m" + str (i), masters [m], 'vagrant', 'vagrant')
        i += 1
        
    i = 0
    for c in computes:
        launchInstallCentos ("c" + str (i), computes [c], 'vagrant', 'vagrant')
        i += 1
        
def createSparkInventory (masters : list, computes: list)-> None:
    result = "\n[master]\n"
    for m in masters : 
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (masters[m])

    result = result + "[nodes]\n"
    for n in computes:
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (computes[n])

    with open ('inventories/spark_base') as istream:
        result = istream.read () + result

    with open ('spark-hosts.ini', 'w') as ostream:
        ostream.write (result);
        

def createKollaInventory (masters : list, computes: list)-> None:
    result = "[control]\n"
    for m in masters : 
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (masters[m])

    result = result + "[network]\n"
    for m in masters:
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (masters[m])


    result = result + "[compute]\n"
    for c in computes:
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (computes[c])


    result = result + "[monitoring]\n"
    for m in masters:
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (masters[m])

    result = result + "[storage]\n"
    for m in masters:
        result = result + "{0!s} ansible_user=root ansible_password=root\n".format (masters[m])

    with open ('inventories/kolla_base') as istream:
        result = result + istream.read ()

    with open ('kolla-hosts.ini', 'w') as ostream:
        ostream.write (result);

def createKubeInventory (masters : list, computes: list) -> None:
    results = "[masters]\n"
    for i in  masters:
        results = results + "{0!s} ansible_user=root ansible_password=root\n".format (masters [i])

    results = results + "[etcds]\n"
    for i in  masters:
        results = results + "{0!s} ansible_user=root ansible_password=root\n".format (masters [i])

    results = results + "[nodes]\n"
    for i in  computes:
        results = results + "{0!s} ansible_user=root ansible_password=root\n".format (computes [i])

    results = results + "[kube-cluster:children]\n"
    results = results + "masters\n"
    results = results + "nodes\n"

    with open ('kube-hosts.ini', 'w') as ostream:
        ostream.write (results)
        
def createInventory (masters : list, computes : list)-> None:
    createKollaInventory (masters, computes)
    createKubeInventory (masters, computes)
    createSparkInventory (masters, computes)
        
def main (args) :
    print ("Will deploy a cluster of {0!s} master(s) and {1!s} compute(s)".format (args.master, args.compute))
    print ("Master description : ")
    print ("    CPU: {0!s}".format (args.cpu_master))
    print ("    RAM: {0!s}".format (args.memory_master))
    print ("    OS: {0!s}".format (args.os))
    print ("Compute description : ")
    print ("    CPU: {0!s}".format (args.cpu_compute))
    print ("    RAM: {0!s}".format (args.memory_compute))
    print ("    OS: {0!s}".format (args.os))
    print ("Provider : {0!s}".format (args.provider))
    
    y = input ("Yes?")
    if (y.upper () != "Y" and y != "" and y.upper () != "YES"):
        return
        
    (masters, computes) = bootCluster (args)
    createInventory (masters, computes)
    if ("ubuntu" in args.os) :
        configureClusterUbuntu (masters, computes)
    elif ("centos" in args.os) :
        configureClusterCentos (masters, computes)

if __name__ == "__main__" :
    parser = argparse.ArgumentParser()
    
    parser.add_argument ("-c", "--compute", help="number of compute", default=1)
    parser.add_argument ("-m", "--master", help="number of master", default=1)
    parser.add_argument ("-os", "--os", help="os of master and compute", default="bento/ubuntu-20.04")
    parser.add_argument ("-cm", "--cpu-master", help="number of cpu for master", default=1)
    parser.add_argument ("-cc", "--cpu-compute", help="number of cpu for computes", default=1)
    parser.add_argument ("-mc", "--memory-compute", help="size of memory for computes (MB)", default=2048)
    parser.add_argument ("-mm", "--memory-master", help="size of memory for computes (MB)", default=2048)
    parser.add_argument ("-p", "--provider", help="size of memory for computes (MB)", default="virtualbox")
    parser.add_argument ("-lf", "--log-file", help="size of memory for computes (MB)", default="/tmp/out.log")
    parser.add_argument  ("-ip", "--base-ip", help="base ip address of the cluster", default="172.16.35.")
        
    args = parser.parse_args()

    setupLogging (args)
    main (args)
