from errors.errorList import *
import logging
from utils.gibberish import gibberish
import os, yaml
import copy

# An action is a object that can be found in a playbook (under a role or a multi role)
class Action :

    def __init__ (self, name : str, roles : dict, log : str, serial : str, hosts : list, gather_fact : bool, var_files : list, vars : list, tasks : list, tag : str, unknown_infos : dict) :
        self.name = name
        self.roles = roles
        self.log = log
        self.serial = serial
        self.hosts = hosts
        self.gather_facts = gather_fact
        self.var_files = var_files
        self.vars = vars
        self.tasks = tasks
        self.tag = tag
        self.unknown_infos = unknown_infos
