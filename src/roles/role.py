from errors.errorList import *
import logging
from utils.gibberish import gibberish
import os, yaml
import copy
from roles.action import *

# A role has a tag (its name)
# And store the various actions, that we found
class Role :

    # @params:
    #    - role: a dictionnary describing the role, this is the description returned by AnsibleParser
    #    - name: the name of the action that has generated this role
    def __init__ (self, name : str, role : dict, actionName : str) :
        logging.info ("New role : " + name + " " + str (type (role)) + " " + actionName)
        self.actions = {}
        self.name = name
        self.actives = {}

        self.writables = {} # TODO
        
        self.localDep = {}
        self.remoteDep = {}
        self.providing = {}
        
        self.import_always = False
        
        self.update (name, role, actionName)


    # Update the role from a role retreived in an ansible playbook
    # @params:
    #    - roles: the list of action returned in the playbook
    #    - name: the name of the action
    # @raise: IllegalStateError, if there are elements in role that are unknown
    def update (self, name : str, actions : dict, actionName : str) -> None:
        logging.Logger.info ("Setup role " + str (name) + " for action -> "  + str (actionName))
        if (not (type (actions) is list)) :
            actions = [actions]
            
        for action in actions : 
            if ("name" not in action) :
                action ["name"] = None

            if ("log" not in action) :
                action ["log"] = None

            if ("serial" not in action) :
                action ["serial"] = None

            if ("hosts" not in action) :
                action ["hosts"] = None

            if ("gather_facts" not in action) :
                action ["gather_facts"] = False

            if ("vars_files" not in action) : 
                action ["vars_files"] = None            

            if ("vars" not in action) : # Idem
                action ["vars"] = None

            if ("tasks" not in action) :
                action ["tasks"] = None

            if ("tags_name" not in action) :
                action ["tags_name"] = "always"

            aux = copy.deepcopy (action)
            for i in ["name", "log", "serial", "hosts", "gather_facts", "vars_files", "vars", "tasks", "tags_name"] :
                del aux [i]
    
        if (actionName in self.actions) : 
            self.actions [actionName] = self.actions [actionName] + [Action (action ["name"], None, action ["log"], action ["serial"], action ["hosts"], action["gather_facts"], action ["vars_files"], action ["vars"], action ["tasks"], action ["tags_name"], aux)]
        else :
            self.actions [actionName] = [Action (action ["name"], None, action ["log"], action ["serial"], action ["hosts"], action["gather_facts"], action ["vars_files"], action ["vars"], action ["tasks"], action ["tags_name"], aux)]
            


    def importAlways (self) :
        self.import_always = True
    
    # Add an action in the list of playable action by the role
    # @params:
    #    - name: the name of the action (eg. config, deploy, ...)
    #    - content: the list of tasks to performed when the action is launched
    # @raise: IllegalStateError, if an action named `name` was already present in the role playbook, but was doing different action
    #         in other terms, if str(content) != str (self.actions[content])
    def addAction (self, name : str, content : list) -> None:
        if (name in self.actions) :
            if (str (self.actions[name]) != str (content)) :
                raise IllegalStateError ("Implicitely erasing the action '" + name + "' is not allowed, use updateAction instead")

        self.actions [name] = content

    # Update the action named `name` in the playbook
    # @params:
    #    - name: the name of the action (eg. config, deploy, ...)
    #    - content: the list of tasks to performed when the action is launched
    # @raise: IllegalStateError, if the action named `name` was unknown initially
    def updateAction (self, name : str, content : list) -> None:
        if (not (name in self.actions)):
            raise IllegalStateError ("Unknown action '" + name + "' use addAction instead")
        
        self.actions [name] = content

    # Set an action active
    # All actions are inactive by default
    # The reason is that ansible parser may return more actions that required
    # So to avoid perform useless tasks, we set them inactive, and active only what is required
    def setActive (self, name : str) -> None:        
        if (not (name in self.actions)) :
            raise IllegalStateError ("Unknown action '" + name)

        logging.Logger.info ("Setting action " + name + " for role " + self.name)
        self.actives[name] = True

    # A role is active if it has a least one active action
    def isActive (self) -> bool: 
        return bool (self.actives) # not isEmpty
        
    # Get an action whose name is `name`
    # @raise: IllegalStateError, if the action is unknown
    def getAction (self, name : str) -> list:
        if (not (name in self.actions)):
            raise IllegalStateError ("Unknown action '" + name + "'")
        return self.actions [name]

    # @return: True iif the action name is found in the known action of the role
    def hasAction (self, name : str) -> bool:
        return name in self.actions


    # Add internal dependencies for the role
    # @params:
    #    - childs: the list of actions depending on parents
    #    - parents: the list of action to perform before the list `childs`
    # @assume: all the actions inside childs and parents are real actions
    def addLocalDependencies (self, childs : list, parents : list) -> None:
        for j in childs :
            if (j in self.localDep) :
                self.localDep [j] = self.localDep [j] + parents
            else :
                self.localDep [j] = parents

    # Add dependencies to providers created in remote roles
    # @params:
    #    - actions: the list of actions that will depend on the provider
    #    - provider: a couple (roleName, providerName)
    # @info: the providers list can be generated by the function addProviders on the role that provides
    def addRemoteDependencies (self, actions : list, provider : (str, str)) -> None:
        for j in actions :
            if (j in self.remoteDep) :
                self.remoteDep [j] = self.remoteDep [j] + [provider]
            else :
                self.remoteDep [j] = [provider]

    # Add providers information, and create a list of provider usable by other roles
    # @params:
    #    - actions: the list of action to perfom before the providing
    # @return: a couple (roleName, providerName), the providerName is automatically generated
    def addProviders (self, actions: list) -> (str, str): 
        providerName = ""
        while (providerName == "" or providerName in self.providing):
            providerName = gibberish (3)

        self.providing [providerName] = actions
        return (self.name, providerName)
        

        
    # Only shows the actions name, and the role important information (hosts, tags, ...)
    # Do not print the tasks (there are quite heavy)
    def __str__ (self) -> str:
        ret = "Role:{0!s}".format (self.name)
        ret = ret + " {" # unmatched "{" in format spec
        ret = ret + "\n    tags:{1!s}\n    logs:{5!s}\n    hosts: {2!s}\n    vars_files: {3!s}\n    vars: {4!s}\n    tasks: [".format (self.name, self.tags, self.hosts, self.vars_files, self.vars, self.log)
        i = 0
        for j in self.actions :
            if (i != 0) :
                ret = ret + ','
            i = i + 1
            ret = ret + "\n        " + j + " : " +  str(len (self.actions [j])) + " " + str (j in self.actives)
            if (j in self.localDep) :
                ret = ret + "\n            after -> " + str (self.localDep [j])
            if (j in self.remoteDep) :
                ret = ret + "\n            wait => " + str (self.remoteDep [j])
        ret = ret + "\n    ]"

        i = 0
        for j in self.providing :
            if (i != 0):
                ret = ret + ","
            ret = ret + "\n    provide => " + j + " after " + str (self.providing [j])
            
        ret = ret + "\n}"            
        return ret

    
    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================               STATES                     ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

    # Create the list of dependencies, where the names are the writable ones
    # Also add the empty dependencies, for the states that don't have any dependency
    # @return: the dictionnary of dependency (close to self.localDep, but with writableNames)
    # @example:
    # ---------
    # print (self.localDep) # {"{'action': 'deploy'}" : ["{'action' : 'pull'}"]}
    # deps = self.createWritableDep ()
    # print (deps) # {'deploy' : ['pull'], 'pull' : []}, (the result depends on toWritable func)
    # ---------
    def createWritableDep (self) -> dict:
        deps = {}
        for z in self.actives :
            if (z in self.localDep) : 
                deps [self.toWritable (z)] = [self.toWritable (i) for i in self.localDep [z]]
            else :
                deps [self.toWritable (z)] = []
        return deps
    
    # Create a gantt list from dependencies
    # @params:
    #    - deps: the dict of dependencies 
    # @return: the list of the tasks names
    # @example:
    # ---------
    # taskDep = {'t1' : ['t2'], 't2': ['t4', 't3'], 't4' : [], 't3' : []}
    # gantt = self.createGanttList (taskDep)
    # print (gantt) # [t4, t3, t2, t1]
    # ---------
    def createGanttList (self, deps: dict)-> list:
        useDeps = copy.deepcopy (deps)
        
        gantList = []
        while (bool (useDeps)) : # not isEmpty
            remove = None
            for j in useDeps:
                if (len (useDeps [j]) == 0) :
                    remove = j
                    break

            if (remove == None) :
                raise IllegalStateError ("Circular dependency " + str(useDeps))
            
            del useDeps [remove]
            for j in useDeps:
                if (remove in useDeps [j]): 
                    useDeps [j].remove (remove)
            
            gantList = gantList + [remove]

        return gantList
    
    # Retreive the init state and final state of a transition
    # @params:
    #    - action: the transition
    #    - states: the dictionnary of states and transitions (const)
    # @return: the couple (x, y) where (x - `action` -> y)
    # @example:
    # ---------
    # states = {'final' : {'init' : ['foo'], 'inter' : ['baz']}, 'inter' : {'init' : ['bar']}, 'init':{}}
    # (a_start, a_end) = self.getTransition ('foo', states)
    # print (a_start) # init
    # print (a_end) # final
    # ---------
    def getTransition (self, action : str, states : dict) -> (str, str) :
        for s in states:
            for j in states [s]:
                if action in states [s][j] :
                    return (j, s)
        return (None, None)


    # Extract the actions `actions` from the state dictionnary states and return them
    # @params:
    #    - actions: the list of actions to extract
    #    - states: a dictionnary of states and transitions (const)
    # @return: (the new dictionnary of states, the extractions containing the init and the final states where the action where leading)
    # @example:
    # -------
    # states = {'final' : {'init' : ['foo'], 'inter' : ['baz']}, 'inter' : {'init' : ['bar']}, 'init':{}}
    # (new_states, extract) = extractActions (['foo', 'bar'], states)
    # print (new_states) # {'final' : {'inter' : ['baz']}, 'inter' : {}, 'init' : {}}
    # print (extract) # {'foo' : ('init', 'final'), 'bar' : ('init', 'inter')}
    # -------
    def extractActions (self, actions : list, states : dict)-> (dict, dict):
        new_states = copy.deepcopy (states)
        ret = {}
        for a_name in actions :
            (a_start, a_end) = self.getTransition (a_name, new_states)
            new_states [a_end][a_start].remove (a_name)

            if (not (bool (new_states [a_end][a_start]))):
                del new_states [a_end][a_start]
            ret [a_name] = (a_start, a_end)
        return (new_states, ret)


    # Create the intermediate states necessary (but maybe reducable) to guarantee the dependencies for the action `action[0]`
    # @params:
    #    - extract: the extraction of the dependencies of `action[0]` (Cf. self.extractActions)
    #    - states: the dictionnary of states and dependencies (strictly disjointed from `extract`, basically the result of self.extractActions)
    #    - action: an action and its previous init and final states ('name' = action [0], 'init' = action [1], 'end' = action [2])
    # @return: [0] = a new state dictionnary, where the extraction is added, as well as the action; [1] = True if empty action where added, False otherwise
    # @warning: the method is not self sufficiant, and must be called in the right order, Cf. resolveStates
    # @example:
    # ----------
    # ----------
    def createIntermediateStates (self, extract : dict, states : dict, action : (str, str, str)) -> (dict, bool):
        # That is the complex part of the algorithm
        # There are several cases

        finalStates = copy.deepcopy (states)
        addedStates = {} # The dictionnary of added states, and for what they where added
        listStates = []
        addEmpty = False
        
        for dep in extract :
            if not extract [dep][1] in addedStates : # The action is the only one (for the moment) that is leading to that state
                stateName = gibberish (2) # We add a new state
                addedStates [extract [dep][1]] = stateName
                finalStates [stateName] = {extract [dep][0] : [dep]}
                listStates = listStates + [stateName]
                
                if (extract [dep][1] != action[2]) : # The action is a dependency of another action, we need a synchronisation (if it is for the final state)
                    if (extract [dep][1] in finalStates and extract [dep][1] != "final") :
                        addEmpty = True
                        finalStates [extract [dep][1]].update ({stateName: ['__empty__']})                        
                # else: In that case the action `action` is the action that will be used as the synchronisation action
                # If the state dictionnary is constructed in the right order, it can be stated that every action beneath the state `action[2]` depend on `action[0]`
                # Cf. resolveStates
                
            elif extract [dep][1] in addedStates : # The action is leading the a synchronisation point with another extracted action
                stateName = addedStates [extract[dep][1]]
                if (extract [dep][0] in finalStates [stateName]) : # there are already action leading to this state from extract [dep][0]
                    finalStates [stateName][extract [dep][0]] = finalStates [stateName][extract [dep][0]] + [dep]
                else : # First time there is a transitions from extract [dep][0] to stateName
                    finalStates [stateName].update ({extract [dep][0] : [dep]})
                # If we are here, and a empty sync is necessary, it already have been added
                                    
        # There is at least one added state, or ... well
        if (len (listStates) == 1) : # Easy case, just add the dependency of action
            finalStates [action [2]].update ({listStates [0] : [action [0]]})
        else : # Need to add a sync state 
            stateName = gibberish (2)
            finalStates [stateName] = {}
            for j in listStates :
                finalStates [stateName].update ({j : ['__empty__']})
            finalStates [action [2]].update ({stateName : [action [0]]})

        return (finalStates, addEmpty)

    # Get all the out transition of a state
    # @params:
    #    - stateName: the name of a state
    #    - states: a dictionnary of states and transitions
    # @return: a list of transition list = [([0] = transition name, [1] = out state name), ...]
    def getOutTransitions (self, stateName : str, states : dict) -> list:
        out = []
        for state in states :
            if stateName in states [state] :
                for j in states [state][stateName] : 
                    out = out + [(j, state)]
        return out
    
    # Remove the useless states
    # A useless state is state that has only one entry transition and one out transition, and out transition is __empty__
    # @params :
    #    - states: a dictionnary of states and transitions
    # @return: a dictionnary of states and transitions
    def simplifyStates (self, states : dict) -> dict:
        new_states = copy.deepcopy (states)
        toRemove = []
        
        for state in new_states :
            deps = new_states [state]
            if (len (deps) == 1 and len (deps [list(deps.keys ())[0]]) == 1) : # If only one entry
                fr_m = list(deps.keys ())[0]
                transition = deps [fr_m][0]
                
                outs = self.getOutTransitions (state, new_states)
                if (len (outs) == 1 and outs [0][0] == "__empty__") : # If only one out and it is __empty__
                    del new_states [outs[0][1]][state] # we delete the dependency from that state 
                    toRemove = toRemove + [state]  # We can't delete it in the for loop 
                    if (fr_m in new_states [outs[0][1]]) : # we skip the state                  
                        new_states [outs [0][1]][fr_m] = new_states [outs [0][1]][fr_m] + [transition]
                    else :
                        new_states [outs [0][1]].update ({fr_m : [transition]})
                elif (len (outs) == 1 and transition == "__empty__") : # If only one in and out is empty
                    del new_states [outs [0][1]][state]
                    toRemove = toRemove + [state]
                    if (fr_m in new_states [outs [0][1]]) :
                        new_states [outs [0][1]][fr_m] = new_states [outs[0][1]][fr_m] + [outs[0][0]]
                    else :
                        new_states [outs [0][1]].update ({fr_m : [outs[0][0]]})
                    
        for state in toRemove :
            del new_states[state]
            
        return new_states
    
    # Compute the different state of the role depending on the internal dependencies    
    # @return: ([0] = a list of state, for each state we have the transitions, and the ancestor state; [1] = need empty transition?)
    # @info: there are at least 2 states, init and final    
    def resolveStates (self) -> (dict, bool):
        actionNames = [self.toWritable (i) for i in self.actions if i in self.actives]
        dependencies = self.createWritableDep ()

        gantt = self.createGanttList (dependencies)
        gantt.reverse () # We start by the task that depend on everything
        
        basicStates = {"final" : {'init': actionNames}, "init" : {}}
        needEmpty = False                
        
        for action_name in gantt :  
            if (len (dependencies [action_name]) != 0) :
                
                (start_state, end_state) = self.getTransition (action_name, basicStates)                
                basicStates [end_state][start_state].remove (action_name)
                if (not bool (basicStates [end_state][start_state])):
                    del basicStates [end_state][start_state]

                (extract_states, extract) = self.extractActions (dependencies [action_name], basicStates)                
                (update_states, update_needEmpty) = self.createIntermediateStates (extract, extract_states, (action_name, start_state, end_state))                
                simplified_states = self.simplifyStates (update_states)                               
                basicStates = simplified_states

                if (update_needEmpty) :
                    needEmpty = True
                
        
        return (basicStates, needEmpty)
    
    # Find the state for which we ensure that every tasks of `action` are done
    # @params:
    #    - states: the current state, computed for internal dependencies
    #    - action: the list of action to perform before providing dependency
    # @info: may add a state and some dependencies, 
    # @TODO: it may be usefull to call self.simplifyStates on the result
    def findFinalState (self, states : dict, action : list) -> (dict, str):
        listState = []        
        
        for dep in action: # We get the final state of all the action
            (start_state, end_state) = self.getTransition (self.toWritable (dep), states)
            if not end_state in listState :
                listState = listState + [end_state]
                
        if (len (listState) == 1) : # Easy, just add the external dependency under that state
            return (states, listState [0])
        else : # Add an empty dependency between all the states and a new state, that will be the external dependency provider
            auxStates = copy.deepcopy (states)
            stateName = gibberish (2) + self.name
            auxStates [stateName] = {}
            for j in listState :
                auxStates [stateName].update ({j : ['__empty__']})
            return (auxStates, stateName)
        
        return (states, "final")

    # Transition a dictionnary of states in a printable dot str
    # @params:
    #    - states: a dictionnary of states and dependencies
    #    - name: the name of the graph (that will be added to the name of each state)
    # @return: a str dot formatted as a digraph
    def toDot (self, states: dict, name : str="G") -> str:
        ret = ("digraph " + name + "{")
        for i in states :
            for j in states [i]:
                for z in states [i][j]: 
                    ret = ret + ("\t" + j + "_" + name + " -> " + i + "_" + name + "[label=" + z + "];\n")
        ret = ret + ("}\n")
        return ret

    
    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================               ANSIBLE DUMPING            ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================        
    
    def dumpTasks (self, outDir : str) -> None:
        if (len (self.actions) != 0) :
            logging.Logger.info ("Dump tasks for role " + str (self.name))
            taskDir = os.path.join (outDir, 'roles/' + self.name + '/tasks/')
            
            self.ensureDir (taskDir)
            yamlMainTaskFile = [{
                'include': '{{ __action__ }}.yml'
            }]

            with open (os.path.join (taskDir, 'main.yml'), 'w') as stream:
                stream.write (yaml.dump (yamlMainTaskFile))

            for i in self.actions :            
                writable = self.toWritable (str (i))
                logging.Logger.info ("Dumping tasks : " + i + " => " + writable + ".yml for role : " + self.name)
                result = []
                for a in self.actions[i] :
                    result = result + a.tasks
                    
                with open (os.path.join (taskDir, writable + ".yml"), 'w') as stream:                    
                    stream.write (yaml.dump (result))        

    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================               MADEUS DUMPING             ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================
    
    # Create the string of the `__init__` function of the class
    def createInitFunction (self, playbook: str)-> str:
        func =          "    def __init__ (self, playbook_dir, inventory, vars_dir):\n"
        func = func +   "        self.playbook_actions = {0!s}\n".format (str (playbook))
        func = func +   "        self.playbook_dir = playbook_dir\n"
        func = func +   "        self.inventory = inventory\n"
        func = func +   "        self.vars_dir = vars_dir\n\n"
        func = func +   "        MadeusComponent.__init__ (self)\n\n"
        return func
       
    # Create the string of the `create` function
    def createCreateFunc (self, states: dict) -> (str, list, bool):
        func =        "    def create (self):\n"
        func = func + "        self.places = ["
        for j in states :
            func = func + "\n            '{0!s}',".format (j)
        func = func + "\n        ]\n\n"
        func = func + "        self.initial_place = 'init'\n\n"

        func = func + "        self.dependencies = {"
        used = []
        for j in self.remoteDep : # action -> use (roleName, provideName)
            for u in self.remoteDep [j]: 
                name = u [0] + "_" + u [1]
                func = func + "\n            '{0!s}': (DepType.USE, ['{1!s}']),".format (name, self.toWritable (j))
                used = used + [{'role': u[0], 'name' : u[1], 'used': name, 'who' : self.name}]

        for j in self.providing :
            (states, state_name) = self.findFinalState (states, self.providing [j])
            func = func + "\n            '{0!s}': (DepType.PROVIDE, ['{1!s}']),".format (j, state_name)
                
        func = func + "\n        }\n\n"
        func = func + "        self.transitions = {"

        needEmpty = False
        for j in states:
            for l in states [j]:
                fr_m = l
                for z in states [j][l] :
                    needEmpty = needEmpty or z == "__empty__"
                    func = func + "\n            '{0!s}': ('{1!s}', '{2!s}', self.{0!s}),".format (z, fr_m, j)

        func = func + "\n        }\n\n"
        
        return (func, used, needEmpty)


    # Create the function associated to an action
    def createActionFunc (self, actionName : str, ansibleVar: str) -> str:
        func =         "    def {0!s}(self):\n".format (actionName)
        func = func +  "        host,ip = get_host_name_and_ip ()\n"
        func = func +  "        logging.debug ('host: %s - ip: %s : action {0!s} of {1!s} start', host, ip)\n".format (actionName, self.name.replace ("-", "_"))
        func = func +  "        return run_ansible (str (Path (self.playbook_dir, self.playbook_actions['{2!s}'])), self.inventory, ['__action__={0!s}'] + {1!s}, self.vars_dir)\n\n\n".format (actionName, ansibleVar, actionName)
        return func


    # Create an empty transition function
    def createEmptyFunc (self) -> str:
        func =         "    def __empty__ (self):\n"
        func = func +  "        host,ip = get_host_name_and_ip ()\n"
        func = func +  "        logging.debug ('host: %s - %ip: %s : action __empty__ of {0!s} start', host, ip)\n".format (self.name)
        func = func +  "        return None" # run_ansible seems to return None
        return func

    
    # Write the madeus python file containing the class of the module
    # @return: the list of using
    def dumpMadeus (self, playbook : str, outDir : str) -> list:
        (states, needEmpty) = self.resolveStates ()

        logging.Logger.dotLog ("{0!s}.dot".format (self.name), dot=self.toDot (states), dir=os.path.join (outDir, "dots"))
        
        initFunc = self.createInitFunction (playbook)
        (createFunc, used, needEmpty) = self.createCreateFunc (states)
        actionNames = [i for i in self.actions if i in self.actives]
        transitions = []
        
        for i in actionNames :
            transitions = transitions + [self.createActionFunc (self.toWritable (i), self.toArray (i))]        

        if needEmpty :
            transitions = transitions + [self.createEmptyFunc ()]
            
        imports =           "import logging\n"
        imports = imports + "from pathlib import Path\n"
        imports = imports + "from concerto.madeus.all import (MadeusComponent, DepType)\n"
        imports = imports + "from utils.extra import run_ansible\n"
        imports = imports + "from utils.extra import get_host_name_and_ip\n"


        className = "\nclass " + self.name.capitalize ().replace ("-", "_") + "(MadeusComponent):\n\n"

        finalStr = imports + className + initFunc + createFunc

        for i in transitions:
            finalStr = finalStr + i

        self.ensureDir (outDir)
        with open (os.path.join (outDir, self.name.replace ("-", "_") + ".py"), "w") as stream:
            stream.write (finalStr)
            
        return used
        

    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================                UTILITIES                 ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================
                
    # Create the dir if it does not exist
    def ensureDir (self, path: str) -> None:
        if not os.path.exists(path):
            os.makedirs(path)


    # Create a name that could be a filename from a string that contains stuff
    # TODO, for the moment I will just store a dictionnaries of gibberish
    def toWritable (self, name: str) -> str:        
        if (':' in name) : 
            i = name.index (':')
            j = name.index ('}')
            
            name = name[i+1:j]
            i = name.index ("'")
            j = name[i+1:].index ("'")
            return '__' + name [i+1:j+i+1] + '__'
        else :
            if (name in self.writables) :                
                return self.writables [name]
            else :
                writable_name = gibberish (3)
                self.writables [name] = writable_name
                return writable_name
        

    def toArray (self, name : str) -> str :
        if (':' in name) :
            b = name.index ('{')
            i = name.index (':')
            e = name.index ('}')
            var = name[b+1:i]
            val = name [i+1:e]

            i = var.index ("'")
            j = var[i+1:].index ("'")
            var = var [i+1:j+i+1]

            i = val.index ("'")
            j = val[i+1:].index ("'")
            val = val [i+1:j+i+1]

            return "['" + var + "=" + val + "']";
        else :
            return '[]'
            
            
    def setToWritable (self, names : dict) :
        self.writables = names
