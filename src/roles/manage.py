from parsing.ansible import AnsibleParser 
from roles.role import *
from roles.multi import *
import yaml, shutil

# Class used to manage roles
# It will store all the roles acquired by the ansible parser
# This will be then used to export the roles in the ansible final playbook
# And for the madeus exports
class RoleManager :

    def __init__ (self, playbook : str, groupVars : list) :
        self.roles = {}
        self.playbook = playbook
        self.groupVars = groupVars

    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================                ROLES                     ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================
        
    # Retreive the role in the ansible playbook, whose name is `roleTag`
    # @params:
    #    - roleTag: the name of the role
    #    - varDict: the external variable passed to ansible
    # @return: A role, or None is the role is not found
    # @raise: IllegalStateError, if either the role or the action for the role are not found
    def getRole (self, roleTag: str, varDict : dict) -> Role:
        logging.Logger.info ("Asking role : '" + str (roleTag) + "' with " + str (varDict))
        if (roleTag in self.roles) : # this part is used to prevent useless parsing
                                     # It is common that Ansible parser return multiple roles, so it may already have been parsed in the past
            if (self.roles [roleTag].hasAction (str (varDict))) :
                logging.Logger.info ("Role '" + str (roleTag) + "' with " + str (varDict) + " found !")
                return self.roles [roleTag]

        ansibleParser = AnsibleParser ()
        roles_ = ansibleParser.parseRoot (self.playbook, self.groupVars, varDict)
        logging.Logger.info ("Found new roles when parsing : " + str (self.playbook) + " -> " + str ([i for i in roles_]))
        
        self.updateRoles (roles_, varDict)
        
        if (roleTag in self.roles) : # this part is used to prevent useless parsing
                                     # It is common that Ansible parser return multiple roles, so it may already have been parsed in the past
            if (self.roles [roleTag].hasAction (str (varDict))) :
                return self.roles [roleTag]
            else :
                raise IllegalStateError (str (self.roles [roleTag]) + " has no action named : " + str (varDict))
        else :
            raise IllegalStateError ("Role : " + roleTag + " with action " + str (varDict) + " not found")


    # @return: all the roles known to the manager
    def getRoles (self) -> dict:
        return self.roles


            
    # Update the list of roles of the manager from a list of roles returned by AnsibleParser
    # @params:
    #    - roles: a dictionnary of roles (almost a yaml content)
    def updateRoles (self, roles : dict, varDict : dict) -> None:
        for i in roles :
            name = i
            
            if (name in self.roles) :
                if (not self.roles [name].hasAction (str (varDict))):
                    self.roles [name].update (name, roles [i], str (varDict))
            else :            
                self.roles [name] = MultiRole (name, roles [i], str (varDict))
                                    

    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================             DEPENDENCIES                ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================
            

    # Load a dependency files, and update the role informations accordingly
    # The file is magically created by a human being
    # @params:
    #    - depPath: the path to the yaml file containing the dependencies
    def loadDependencies (self, depPath: str)-> None:
        with open (depPath, 'rb') as stream:
            try:
                yamlContent = yaml.load (stream, Loader=yaml.FullLoader)
                self.parseDepFile (yamlContent)
            except yaml.YAMLError as exc :
                print (exc)
                raise IllegalStateError (depPath + " : Invalid yaml file")


    # Parse the content of a dependency file
    # @params:
    #    - depContent: the yaml formated content of a dependency file
    def parseDepFile (self, depContent : dict) -> None:
        for role in depContent : # Array of {'tags':"", "vars": [], "depend": [{'tags':"", "vars":[]}]}            
            roleName = role ["tags"]
            del role ["tags"]
            
            roleActionList = []
            if ("vars" in role) :
                for i in role ["vars"] :
                    r = self.getRole (roleName, i)
                    r.setActive (str (i))
                    roleActionList = roleActionList + [str(i)]
                del role ["vars"]
            else:
                r = self.getRole (roleName, {})
                r.setActive (str ({}))
                roleActionList = [str({})]

            if ("depend" in role) :
                for dep in role ["depend"] :
                    depName = dep ["tags"]
                    del dep ["tags"]
                    
                    depActionList = []
                    if ("vars" in dep) :
                        for i in dep ["vars"] :
                            d = self.getRole (depName, i)
                            d.setActive (str (i))
                            depActionList = depActionList + [str(i)]
                        del dep ["vars"]
                    else :
                        d = self.getRole (depName, {})
                        d.setActive (str ({}))
                        depActionList = [str({})]
                    if (bool (dep)) : # not isEmpty
                        raise IllegalStateError ("Unknown element : " + str (dep))
                                    

                    if (depName == roleName):
                        self.roles [roleName].addLocalDependencies (roleActionList, depActionList)
                    else :
                        providers = self.roles [depName].addProviders (depActionList)
                        self.roles [roleName].addRemoteDependencies (roleActionList, providers)
                del role ["depend"]
                                
            if (bool (role)): # not isEmpty
                raise IllegalStateError ("Unknown element : " + str (role))


    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================                  DUMPING                 ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

    # Create the string of the `__init__` function of the assembly    
    def createInitFunc (self) -> str: 
        func =        "    def __init__(self, playbook_dir, inventory, vars_dir):\n"
        func = func + "        self.playbook_dir = playbook_dir\n"
        func = func + "        self.inventory = inventory\n"
        func = func + "        self.vars_dir = vars_dir\n\n"
        func = func + "        MadeusAssembly.__init__ (self)\n\n"
        
        return func

    # Create the string of the `create` function
    def createCreateFunc (self, using : list)-> str:
        func =        "    def create(self):\n"
        func = func + "        self.components = {"
        
        for i in self.roles:
            if (self.roles [i].isActive ()) :
                func = func + "\n            '{0!s}': {1!s}(self.playbook_dir, self.inventory, self.vars_dir),".format (i, i.capitalize ().replace("-", "_"))
        func = func + "\n        }\n\n"
        func = func + "        self.dependencies = ["

        for z in using :
            for i in z : 
                func = func + "\n            ('{0!s}', '{1!s}', '{2!s}', '{3!s}'),".format (i ["role"], i["name"], i["who"], i["used"])
        func = func + "\n        ]\n\n"
        return func        
    
    # Dump the main assembly file
    # @params:
    #    - name: the name of the assembly class
    #    - outDir: the out directory
    #    - using: the list of inter modules dependencies
    #
    def dumpMainAssembly (self, name: str, outDir : str, using: list) -> None:
        imports = "from concerto.madeus.all import MadeusAssembly\n\n"
        base = os.path.basename (outDir)
        
        for i in self.roles :
            if (self.roles [i].isActive ()):
                imports = imports + "from {0!s}.{1!s} import {2!s}\n".format (base, i.replace ('-', '_'), i.capitalize ().replace ('-', '_'))

        className = "\n\nclass {0!s}(MadeusAssembly):\n\n".format (name)
        initFunc = self.createInitFunc ()
        createFunc = self.createCreateFunc (using)

        finalStr = imports + className + initFunc + createFunc
        
        with open (os.path.join (outDir, name.lower () + ".py"), 'w') as stream:
            stream.write (finalStr)
        

    # Dump the python main, and some utilities used by madeus
    # @params:
    #    - outDir: the directory in which the file 'assembly-runner.py' will be written
    #    - assemblyDir: the directory in which the assembly has been written
    #    - ansibleDir: the path of the ansible directory
    def dumpAssemblyRunnerAndUtils (self, outDir : str, ansibleDir : str, vars_dir : str=None) -> None:
        self.copyTree ("./utils", os.path.join (outDir, "utils"))
        self.copyFile ("./runners/basic-runner.py", os.path.join (outDir, "main.py"))
        # There is probably a configuration already, we don't overwrite it, it is probably more suitable than the standard one
        # But we have to move it at the right place
        if (not os.path.exists (os.path.join (os.path.join (outDir, "ansible"), "ansible.cfg"))) : 
            self.copyFile ("./configs/ansible.cfg", os.path.join (outDir, "ansible.cfg"))
        else :
            self.copyFile (os.path.join(os.path.join (outDir, "ansible"), "ansible.cfg"), os.path.join (outDir, "ansible.cfg"))
            
        self.copyFile ("./utils/__init__.py", os.path.join (os.path.join (outDir, "assemblies"), "__init__.py"))
        if (vars_dir != None) : 
            self.copyTree (vars_dir, os.path.join (outDir, "vars_dir"))

    # The tasks in the main playbook have to be done before each role, (they basically do groups, and fact gathering)
    # So, we tell to each roles that will be dumped to import the always tasks, and then execute themselves
    # @params:
    #    - ansibleDir: the path o the out ansible directory
    def dumpAlwaysRole (self, ansibleDir : str) -> None:
        if ("always" in self.roles) :
            for i in self.roles :
                if i != "always" :
                    self.roles [i].importAlways ()
            self.roles ["always"].setAllActives ()
        
    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================                  COPY                    ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

    # Copy the content of a file into another file
    # @params:
    #    - src: the source file path
    #    - dst: the out file path
    def copyFile (self, src : str, dst : str)-> None: 
        in_file = open (src, 'rb') # in byte mode, because otherwise python is stupid and try to decode 
        out_file = open (dst, 'wb')
        out_file.write (in_file.read ())
        in_file.close ()
        out_file.close ()        
        
    # Copy a dir into another one
    # @params:
    #    - src: the path of the source directory
    #    - dst: the path of the destination directory 
    def copyTree(self, src : str, dst : str)-> None:
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                self.ensureDir (d)
                self.copyTree (s, d)
            else:
                self.ensureDir (os.path.dirname (d))
                self.copyFile (s, d)

                
    # Create the dir if it does not exist
    def ensureDir (self, path: str) -> None:
        if not os.path.exists(path):
            os.makedirs(path)

                
    # Copy all the files in ansible directory that may be used for configuration
    # @params:
    #    - outDir: the output directory of the new ansible 
    def copyAllConfigFiles (self, outDir : str) -> None:
        ansiblePath = os.path.dirname (self.playbook)
        self.copyTree (ansiblePath, outDir)
    
