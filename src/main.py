
# First step is to retreive the tasks of all the ansible roles

import yaml
import argparse
import glob
import os, shutil
import ntpath
from typing import Any

from roles.manage import RoleManager
import logging, sys

DEBUG_JINJA_LEVEL = 10
DEBUG_DOT_LEVEL = 9
DEBUG_LEVELS = []

def jinjaLog (message, *args, **kws):
    if DEBUG_JINJA_LEVEL in DEBUG_LEVELS:
        logging.Logger.debug (message) 

def dotLog (message, *args, **kws):
    if DEBUG_DOT_LEVEL in DEBUG_LEVELS : 
        logging.Logger.debug ("Dumping dot file in " + os.path.join (kws["dir"], message))
        if not os.path.exists(kws["dir"]):
            os.makedirs(kws["dir"])
            
        with open (os.path.join (kws["dir"], message), 'w') as stream :
            stream.write (kws["dot"])

        

def setupLogging (args: Any) :
    global DEBUG_LEVELS

    # If you want to debug jinja rendering
    #   DEBUG_LEVELS = DEBUG_LEVELS + [DEBUG_JINJA_LEVEL]

    # If you want to debug states rendering (and dump the dot files in /tmp/{role name}.dot)  
    DEBUG_LEVELS = DEBUG_LEVELS + [DEBUG_DOT_LEVEL]
    
    logging.basicConfig(filename=args.log_file, level=logging.DEBUG)
    
    logging.Logger = logging.getLogger(__name__)
    logging.Logger.jinjaLog = jinjaLog
    logging.Logger.dotLog = dotLog

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logging.Logger.addHandler(handler)
    
        
def main (args: Any) :
    if (os.path.exists (args.out_dir)) :
        txt = input ("Directory " + args.out_dir + " exists, do you want to remove it before starting ? (y/N)")
        if (txt.upper () == "Y"):
            logging.Logger.info ("Removing directory " + str (args.out_dir))
            shutil.rmtree (args.out_dir)
        
    
    
    manager = RoleManager (args.playbook, args.group_vars.split (' '))
    manager.loadDependencies (args.config)
    roles = manager.getRoles ()
    
    manager.copyAllConfigFiles (os.path.join (args.out_dir, "ansible"))
    manager.dumpAlwaysRole (os.path.join (args.out_dir, "ansible"))
    
    using = []
    for i in roles:
        if (roles[i].isActive ()): 
            playbook = roles [i].dumpPlaybook (os.path.join (args.out_dir, "ansible"))
            using = using + [roles [i].dumpMadeus (playbook, os.path.join (args.out_dir, "assemblies"))]

    manager.dumpMainAssembly ("MMadeusAssembly", os.path.join (args.out_dir, "assemblies"), using)
    manager.dumpAssemblyRunnerAndUtils (args.out_dir, os.path.join (args.out_dir, "ansible"))


if __name__ == "__main__" :
    parser = argparse.ArgumentParser()

    parser.add_argument("playbook", help="ansible playbook")
    #parser.add_argument ("inventory", help="ansible inventory")
    
    parser.add_argument ("-c", "--config", help="ansible config file", default="config.yml")
    parser.add_argument ("-l", "--log_file", help="logging file", default="/tmp/out.log")
    parser.add_argument ("-g", "--group_vars", help="group vars directory", default="")
    parser.add_argument ("-o", "--out_dir", help="output directory", default=".result/")

    args = parser.parse_args()
    
    setupLogging (args)
    main (args)
    
