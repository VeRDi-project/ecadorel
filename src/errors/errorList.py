
class IllegalStateError (BaseException) :

    def __init__ (self, msg : str) :
        self.msg = msg
        super ().__init__ ("IllegalStateError (" + msg + ")")

class UnknownElement (BaseException) :

    def __init__ (self, msg : str) :
        self.msg = msg
        super ().__init__ ("UnknownElement (" + msg + ")")
