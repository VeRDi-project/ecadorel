import os
import yaml
from typing import Any
from errors.errorList import *
import logging
import copy
from jinja2 import Template
import jinja2
import importlib
import glob
import sys, inspect

class AnsibleParser :

    def __init__ (self):
        self.ansibleRoot = ""
        self.playbook = ""
        self.inventory = ""
        self.config = {}
        self.plugins_found = []
        
        self.jinjaEnv = None


    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================                MAIN                      ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

    
    # Open the ansible playbook, inventory, and retreive all the roles and actions that would have been performed by ansible 
    # @params:
    #   - playbook: the path of the ansible playbook yaml file
    #   - inventory: the path of the inventory file (in ini format)
    #   - configPath: the path of the yaml file containing all the human configurations
    # 
    def parseRoot (self, playbook : str, groupVars : list, externalVars : dict) -> dict:
        self.ansibleRoot = os.path.dirname (playbook)
        self.playbook = playbook

        self.setupJinja ()                    
        self.plugins_found = self.loadLibrary (os.path.join (self.ansibleRoot, "library")) # Debug purpose only? see comments in parseTaskList:no_questions

        if (groupVars != [""]) : 
            groupVars = ["group_vars/all.yml"] + groupVars
        else :
            groupVars = ["group_vars/all.yml"]
        
        # Each human config is an ansible call, and for each one of them the activated roles/tasks are different
        # We need to get all of them
        self.config = copy.deepcopy (externalVars)
            
        # Maybe the configuration (default macros) depends on the humanConfig
        for i in groupVars :
            groupVarsPath = os.path.join (self.ansibleRoot, i)
            self.config = self.loadGroupVars (self.config, groupVarsPath)
            
        # Parsing the playbook
        with open (playbook, 'rb') as stream :
            try :
                yamlContent = yaml.load (stream, Loader=yaml.FullLoader)                    
                roles = self.parsePlaybook (os.path.basename (playbook), yamlContent, self.config)
            except yaml.YAMLError as exc :
                print (exc)
                raise IllegalStateError ("site.yml : Yaml not valid")
                        
        return roles

    
    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================             INVENTORY                    ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

    # Read an inventory file, this kind of file is in ini format [doc](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
    # The inventory file is used to create the initial roles
    # @params:
    #    - path: the path of the inventory file
    # @returns: a dictionnary of the groups, and the revert dictionnary for faster access
    # @return_example: ({'control': {'localhost' : {'ansible_connection': 'local'}}, 'compute': {'localhost' : {}}}, {'localhost': ['control', 'compute']})
    def readInventory (self, path : str) -> (dict, dict):
        inv = open (path, 'r')
        current_group = ""
        current_group_type = ""

        parents = {}
        groups = {}
        groupVariables = {}

        
        for line in inv :
            s_line = line.strip ()
            
            if (len (s_line) == 0 or s_line [0] == '#') :
                continue
            if (s_line [0] == '[') :
                (c, ct) = self.parseGroupName (s_line)
                current_group_type = ct
                current_group = c
                
                groups [current_group] = {}
                parents [current_group] = []
            
            elif (current_group_type == "") : # host names
                currentHost = self.parseHostName (s_line)
                groups [current_group].update (currentHost)
            elif (current_group_type == "children") :
                groupName = s_line
                
                # of course the groups can be described in any orders, tsss...
                parents [current_group] = parents [current_group] + [groupName]

        groups = self.collapseParenting (parents, groups, groupVariables)
        hosts = self.computeHostInfos (groups)
        (groups ["all"], groups ["ungrouped"]) = self.constructDefaultGroups (hosts)        
        return (groups, hosts)

    # Empty the parent dictionnary, and updates the group informations
    # This means that the host present in a group, will also be present in the parent group
    # @params:
    #    - parents: a parent tree
    #    - groups: the informations we have on the groups for the moment
    #    - groupVariables: the list of variables to apply to the hosts contained in a group
    # @returns: a dictionnaries of group, where updates are made
    # @raise: IllegalStateError, if there are circular parent dependencies
    def collapseParenting (self, parents : dict, groups : dict, groupVariables : dict) -> dict:
        while bool (parents) : # Python !!!! what are you doing?! isEmpty would be great
            leafs = []
            for i in parents : # get all the leafs of the tree
                if (len (parents[i]) == 0) :
                    leafs = leafs + [i]

            for l in leafs : # and make them fold the tree
                if (l in groupVariables) : 
                    groups [l] = self.applyVariables (groups [l], groupVariables [l])
                
                for i in parents :
                    if (l in parents [i]) :
                        groups [i].update (groups [l])
                        parents [i].remove (l)                
                del parents [l]
        return groups

    
    # Add variables to the hosts of the group
    # If they already had the variable, it came from children, so there is no updates (see ansible doc, on variable inheritance)
    # cite: "A child group’s variables will have higher precedence (override) a parent group’s variables"
    def applyVariables (self, group : dict, vars : dict) -> dict:
        pass # TODO
    
    # Reverse the group list, to know to which group a host is associated easily
    def computeHostInfos (self, groups : dict) -> dict :
        hosts = {}
        for g in groups :
            for h in groups [g]:
                if (h in hosts) :
                    hosts [h] = hosts[h] + [g]
                else :
                    hosts [h] = [g]
        return hosts

    # In ansible there two default groups 'all' and 'ungrouped'
    # In all, there are all hosts
    # In ungrouped, there are the hosts that have no group assignment (other than 'all')
    # @params:
    #    - hosts: the list of hosts (result of computeHostInfos)
    # @return: the two default groups
    def constructDefaultGroups (self, hosts : list) -> (dict, dict):
        all = {}
        ungrouped = {}
        for h in hosts :
            if len (hosts [h]) == 0 :
                ungrouped [h] = {}
            all [h] = {}
        return (all, ungrouped)
    
    # Parse a group name
    # It can be just a name, and thus the following lines are host names, example -> [control]
    # It can be a name followed by :children, and thus the following lines are group names -> [control:children]
    # It can be a name following by :vars, and thus the following lines are variables -> [control:vars]
    # @params :
    #    - line: the line containing the name, assumed to start with '['
    # @return: (the group name, the group type (i.e. children, vars, "")
    def parseGroupName (self, line : str) -> (str, str):
        if (':' in line) :
            i = line.index (':')
            return (line [1:i], line [i+1:-1])
        else:
            return (line [1:-1], "")                        
    

    # Parse a host name line
    # It can be just a name, or a name followed by variables
    # the variable are formatted as follows : varname=value varname2=value2 ...
    # @params:
    #    - the line containing the host name, (and variable optionnaly)
    # @returns: a dictionnary of where the host is associated to its variables:
    # @return_example: {'localhost' : {'ansible_connection': 'local'}}
    # @assume: the line is already stripped 
    def parseHostName (self, line : str) -> dict :
        if (' ' in line) :            
            i = line.index (' ')
            name = line[:i]
            line = line [i+1:].strip ()
            varDict = {}
            while len (line) != 0 :
                var_l = ''
                if ' ' in line :
                    i = line.index (' ')
                    var_l = line [:i]
                    line = line [i+1:].strip ()
                else :
                    var_l = line
                    line = ''
                j = var_l.index ('=')
                varDict [var_l[:j]] = var_l[j+1:]
            return {name: varDict}
        else :
            return {line: {}}
        

    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================             PLAYBOOK                     ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

    
    # Parse a playbook yaml content
    # @params :
    #   - book: the yaml loaded content of a playbook file 
    def parsePlaybook (self, name : str, book : list, state : dict) -> list:
        logging.info ("Parsing playbook : " + name)
        restorable_state = state
        state = copy.deepcopy (state)
        
        result = {}
        for y in book :
            current = self.parsePlaybookObject (y, state)
            if (type (current) is list) :
                for i in current : 
                    result = self.addRole (result, i)
            elif (current != None and current != {}) :
                for r in current :
                    for i in current [r] :
                        result = self.addRole (result, i)
                    
            state = restorable_state
            
        return result
            
            
    # Parse an object that can be found inside a playbook
    # @params:
    #   - obj: the object found in the playbook
    def parsePlaybookObject (self, obj : dict, state : dict)-> dict:
        result = []
        
        # name is just a log text
        # serial is used to run in parallel
        # gather_facts
        # vars : variable to add to the state before executing the object
        # vars_files : location of the files containing variables to add to the state
        no_question = ["name", "serial", "hosts", "gather_facts", "vars_files", "vars", "become"]        
        current_result = {"tags": "always"}
        at_least_something = False
        
        for key in obj :                    
            if (key in no_question) :
                current_result [key] = obj[key]
            elif (key == "import_playbook") :
                name = os.path.join (self.ansibleRoot, obj[key])                
                with open (name) as stream :
                    try :
                        yamlContent = yaml.load (stream, Loader=yaml.FullLoader)
                        result = self.parsePlaybook (obj[key], yamlContent, state)
                        if (len (result) != 0) :
                            return result
                        else :
                            return None
                    except yaml.YAMLError as exc :
                        print (exc)
                        raise IllegalStateError (obj[key] + " : Yaml no valid")
            elif (key == "tasks") : # List of tasks
                current_result ["tasks"] = self.parseTaskList (self.ansibleRoot, obj[key], state) # we are at the root
                if (len (current_result ["tasks"]) != 0) :
                    at_least_something = True                    
            elif (key == "tags") : # the tags are used by ansible to know if the current element must be executed or not
                if (type (obj[key]) is list) :
                    current_result ["tags"] = obj[key][0] # TODO, better
                else : 
                    current_result ["tags"] = obj[key]
            elif (key == "roles") : # It can appear only once
                roles = self.parsePlaybookRole (obj[key], state)
                if (not roles == None) :
                    for r in roles:                    
                        if "tags_name" in roles [r] :
                            current_result ["tags"] = roles [r]["tags_name"]
                        current_result ["sub_roles"] = roles
                    at_least_something = True                        
            else :
                raise UnknownElement ("Playbook object : " + key)


        if (at_least_something) :
            result = result + [current_result]
        
        return result

    def addRole (self, result : dict, role : dict) -> dict:
        if ("tags" in role) :
            if ((role ["tags"]) in result) :
                result [role["tags"]] = result [role["tags"]] + [role]
            else :
                result [role["tags"]] = [role]
        else :
            if ("always" in result) :
                result ["always"] = result ["always"] + [role]
            else:
                result ["always"] = [role]
        return result

    # Retreive a list of host, from a list of groups
    # @params:
    #    - hosts: the list of groups (it can be just a group name)
    #    - groups: the known groups
    # @return: a list of hosts (with variables)
    # @return_example: {'localhost': {'ansible_connection': 'local'}}
    def getHostList (self, hosts : list) -> dict:
        if (type (hosts) is list) :
            result = {}
            for h in hosts :
                if (h in groups) : 
                    result.update (groups [h])
            return result
        else :
            if (hosts in groups) :
                return groups [hosts]
            else :
                return {}

    # Read a vars files    
    def readVarsFiles (self, paths : str) -> dict:
        if (not type (paths) is list) :
            paths = [paths]
        result = {}
        for p in paths :
            path = os.path.join (self.ansibleRoot, p)
            with open (path, 'rb') as stream :
                try :
                    result.update (yaml.load (stream, Loader=yaml.FullLoader))
                except yaml.YAMLError as exc :
                    raise IllegalStateError (path + " : Invalid yaml file")
        return result
        
            
    # Parse roles from a playbook
    # @params:
    #    - roles: a list of role description (in yaml format)
    #    - state: the current state
    #    - groups: the current groups
    #    - hosts: the list of hosts on which the role will be applied
    def parsePlaybookRole (self, roles : list, state : dict) -> dict :
        result = {}
        for r in roles :
            current_result = {}
            add = True
            when = []            
            if ("when" in r) :
                (when_, add_) = self.testWhen (r ["when"], state)
                when = when
                add = add_                
                
            if (add) :                
                if (when != []) :
                    current_result ["when"] = when
                for key in r :
                    if (key == "role"):
                        rolePath = os.path.join (os.path.join (self.ansibleRoot, "roles"), r [key] + "/tasks/main.yml")                        
                        with open (rolePath) as stream :
                            try :
                                yamlContent = yaml.load (stream, Loader=yaml.FullLoader)
                                current_result["tasks"] = self.parseTaskList (os.path.dirname (rolePath), yamlContent, state)
                                if (len (current_result ["tasks"]) == 0) : # the other elements are useless if there is no tasks inside the role
                                    current_result = {}
                                    break
                            except yaml.YAMLError as exc :
                                print (exc)
                                raise IllegalStateError (obj[key] + " : Yaml no valid")
                        current_result["tags"] = r[key]
                    elif (key == "tags") :
                        if (type (r[key]) is list) : 
                            current_result ["tags_name"] = r [key][0]
                        else :
                            current_result ["tags_name"] = r [key]
                    elif (key == "when") : # already validated
                        current_result ["when"] = r [key]
                    elif (key == "name") :
                        current_result ["name"] = r [key]
                    else :
                        current_result [key] = r [key]
                        
                if (current_result != {}) :
                    if ("tags_name" not in current_result) :
                        current_result ["tags_name"] = r["role"]
                    if (current_result ["tags"] in result) : 
                        result [current_result["tags"]].update (current_result);
                    else :
                        result [current_result["tags"]] = current_result
                    
        if (len (result) != 0) :
            return result
        else :
            return None

    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================               TASKS                      ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================        
    

    # Parse a list of tasks found inside a playbook
    # @params:
    #    - tasks: the list of tasks (in yaml loaded list)
    #    - hosts: the list of hosts on which the tasks will be executed
    def parseTaskList (self, path : str, tasks : list, state : dict) -> list :
        if (tasks == None) :
            return []
        
        result = []
        restorable_state = state
        
        no_question = ["name", "tags",  "setup", "become", "command", "group_by", "local_action",
                       "register", "changed_when", "failed_when", "stat", "docker_image_info",
                       "run_once", "ignore_errors", "vars", "getent", "wait_for", "group", "user",
                       "fail", "file", "template", "notify", "with_first_found", "systemd",
                       "with_dict", "meta", "merge_configs", "sysctl", "copy", "merge_yaml",
                       "with_fileglob", "set_fact", "no_log", "check_mode", "shell", "args", "debug",
                       "lineinfile", "until", "retries", "delay", "find", "git", "mount", "delegate_facts",
                       "assemble", "loop_control", "with_together", "environment", "delegate_to", "assert",
                       "with_nested", "with_sequence", "loop", "with_subelements", "with_items", "get_url"] + self.plugins_found

        at_least_something = False
        for obj in tasks :
            add = True
            current = {}
            if ("when" in obj) :
                (when_, add_) = self.testWhen (obj["when"], state)
                add = add_
                current ["when"] = obj["when"]
                del obj["when"]
                    
            if (add) :
                for key in obj :
                    if (key == "tags"):
                        current [key] = obj [key]
                    elif (key in no_question) : # this may seems ugly, but the idea is to avoid unknown element type
                        # this is for debugging, in the futur, it can be changed by an simple else: close
                        current [key] = obj[key]
                        at_least_something = True                    
                    elif (key == "block") : # Task block are used to enclose a condition for a list of tasks
                        tasks = self.parseTaskList (path, obj[key], state)
                        if (len (tasks) != 0) :
                            current ["block"] = tasks
                            at_least_something = True
                    elif (key == "import_tasks" or key == "include_tasks" or key == "include") :
                        try :
                            val = self.evaluateValue (obj[key], state)
                            if (val != '') :
                                current [key] = val
                            else :
                                current [key] = obj[key]
                        except IllegalStateError as ex :
                            current [key] = obj[key]
                        at_least_something = True
                    elif (key == "import_role" or key == "include_role") : # merge the tasks TODO
                        try :
                            val = self.evaluateValue (obj[key], state)
                            if (val != '') :
                                current [key] = val
                            else :
                                current [key] = obj[key]
                        except IllegalStateError as ex :
                            current [key] = obj[key]
                        at_least_something = True
                        pass # current [key] = self.parsePlaybookRole ([obj[key]], state)
                    else :
                        raise UnknownElement ("Task list : " + key + " " + str (obj[key]))
                
                if (current != {} and at_least_something):
                    result = result + [current]
        return result

    # parse a with item element
    def parseTaskWithItems (self, task : Any, state : dict) -> list:
        result = []
        if (not (type (task) is list)) :
            task = [task]
        
        for i in task :
            if (type (i) is str) : 
                result = result + [self.evaluateValue (i, state)]
            else :
                current = {}
                for key in i :
                    current [key] = self.evaluateValue (i [key], state)
                result = result + [current]
        return result


    # Parse a with_dict element
    def parseTaskWithDict (self, task : Any, state : dict) -> list:
        result = []
        return self.evaluateValue (task, state)
    
    # Parse a group by task
    # We create the groups if we can, but we let the group by in the result
    # 
    def parseTaskGroupBy (self, task : dict, state : dict) -> None:
        result = {}
        if ("key" in task) :
            try : 
                groupKey = self.evaluateValue (task ["key"], state, failure=False)
                groups [groupKey] = hosts
                return groupKey
            except IllegalStateError as err :
                pass
            
            return task
        else :
            raise IllegalStateError ("I don't know how to treat that : " + str (task))        
        
    # Try to evaluate the result of a condition
    # If the condition is true, it will return the list of condition, and True
    # otherwise an empty list and False
    def testWhen (self, test : Any, state : dict) -> (list, bool) :
        if (type (test) is str) :
            val = self.evaluateValue ("{{ " + test + " }}", state)
            if (val == "False") :
                return ([], False)
            else: 
                return ([test], True)
        else :
            add = []
            for w in test :
                val = self.evaluateValue ("{{ " + w + " }}", state)
                if (val == "False"):
                    return ([], False)
                else :
                    add = add + [w]
            return (add, True)
    


    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================      ANSIBLE CONFIGURATION               ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================

        
    # Load all configuration element that can be loaded
    # the group var file is loaded as well, it will be used to reduce the size of the playbook
    def loadGroupVars (self, configFileContent : dict, groupVarsPath : str) -> dict:
        config = {}
        config.update (configFileContent)
        
        if (os.path.exists (groupVarsPath)) : 
            with open (groupVarsPath, 'rb') as stream :
                try :
                    yamlContent = yaml.load (stream, Loader=yaml.FullLoader)
                    config.update (self.parseGroupVars (yamlContent, config))
                except yaml.YAMLError as exc :
                    print (exc)
                    raise IllegalStateError (groupVarsPath + " : Invalid yaml file")
        else :
            logging.Logger.warning ("Group var file : " + str (groupVarsPath) + " not found ")
            
        #
        # Mmh, maybe it is safer to let everything for ansible, and just replace what's inside the configuration file
        # But we need it to enable only the roles we want
        
        return config

    # Retreive the name of all the library plugins
    # They are used in tasks, do we need to know that we will ignore them
    # Mostly to help debug, as we, for the moment, do not ignore the unknown keys
    def loadLibrary (self, path : str) -> list:
        ignore = []
        for py in glob.glob (os.path.join (path, "*.py")) :
            ignore = ignore + [os.path.splitext (os.path.basename (py))[0]]
        return ignore
    

    # Parse the group var file
    def parseGroupVars (self, groupVars : dict, config : dict) -> dict:
        for key in groupVars :
            config [key] = self.evaluateValue (groupVars [key], config)
    
        return config


    # ==============================================================================================================================
    # ==============================================================================================================================
    # ==========================================               JINJA2                     ==========================================
    # ==============================================================================================================================
    # ==============================================================================================================================


    # Setup the jinja environment
    # Load the filter defined by ansible
    # The filter plugins are located in filter_plugin dir, we assume that all the class that are located there are filters
    def setupJinja (self) -> None: 
        self.jinjaEnv = jinja2.Environment (extensions=['jinja2.ext.i18n'])
        # path = os.path.join (self.ansibleRoot, "filter_plugins/*.py")               
        # for f in glob.glob (path) :
        #     sys.path.append (os.path.join (ansiblePath, "filter_plugins"))
        #     modName = os.path.basename (os.path.splitext (f)[0])
        #     logging.info ("Loading filter module : " + modName + " in file " + f)
        #     mod = __import__(modName)
        #     for name, obj in inspect.getmembers (sys.modules[modName]):
        #         if (inspect.isclass (obj)) :
        #             x = obj ()
        #             print (x)
        #             self.jinjaEnv.filters.update (x.filters ())
        #
        # Well, all that is great but, creates way more problems that it solves, we will let ansible resolves its own filters when it will be executed
        # TODO, remember to copy the filter plugins in the output ansible dir
        
        self.jinjaEnv.filters ["bool"] = self.myBoolFilter


    # Use jinja2 to evaluate templates values
    # The values are evaluated thanks to the configuration file
    # Everything is logged for debugging `--log_file=`
    # @params:
    #    - value: the template value to render (if it is a str, otherwise, it will be returned as it is)
    #    - state: the current state of the configuration
    # the config can be updated by the playbook, so it is no necessarily equal to self.config, but self.config is a subset of state
    # When evaluating, if it fails, we assume that this is a value that will be evaluated by ansible later on
    def evaluateValue (self, value : Any, state : dict, *args, **kwargs) -> Any:            
        authorizedFailure = kwargs.get ("failure", True)
        if (type (value) is str) :
            logging.Logger.jinjaLog ("Rendering template value : " + value)
            try : 
                template = self.jinjaEnv.from_string (value)
                res = template.render (state)
                if (res == "") : # empty is assumed to be fake success, jinja does not raise error if the value is not in dict, I don't know why
                    logging.Logger.jinjaLog ("No result")
                    if (not authorizedFailure) :
                        raise IllegalStateError ("Mandatory value failed : " + str (value))
                    return value
                logging.Logger.jinjaLog ("Result : " + res)
                return res
            except jinja2.exceptions.UndefinedError as exc :
                    logging.Logger.jinjaLog ("Failed to render template : " + str (exc))
                    if (not authorizedFailure) :
                        raise IllegalStateError ("Value compilation failed : " + str (exc) + " " + str (value))
            except jinja2.exceptions.TemplateAssertionError as exc :
                logging.Logger.jinjaLog ("Failed to render template : " + str (exc))
                if (not authorizedFailure) :
                    raise IllegalStateError ("Value compilation failed : " + str (exc) + " " + str (value))
                
        return value


    # Filter for jinja2 templates, when | bool
    def myBoolFilter (self, value : str) -> bool:
        if (value == "yes" or value == "True") :
            return True
        else :
            return False
