# -*- coding: utf-8 -

import os

# PATH constants
MAD_PATH = os.path.abspath(
  os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
SYMLINK_NAME = os.path.abspath(os.path.join(os.getcwd(), 'current'))
ANSIBLE_DIR = os.path.join(MAD_PATH, 'ansible')

