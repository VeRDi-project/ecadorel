
import socket
from pathlib import Path
from subprocess import run
import os

from .constants import (ANSIBLE_DIR, SYMLINK_NAME)



def get_host_name_and_ip():
    try:
        host_name = socket.gethostname()
        host_ip = socket.gethostbyname(host_name)
        return host_name, host_ip
    except:
        print("Unable to get Hostname and IP")


def run_ansible(playbook, inventory, config_vars=[], vars_dir=None, tags=None):
    kolla_vars_file = str(Path(ANSIBLE_DIR) / "group_vars" / "all.yml")
    vars_dir = vars_dir or Path(SYMLINK_NAME)
    global_file = str(Path(vars_dir) / "globals.yml")
    pass_file = str(Path(vars_dir) / "passwords.yml")
    rabbit_vars = str(Path(vars_dir) / "rabbit_vars.yml")

    if (os.path.exists (kolla_vars_file)):
        config_vars.append("@" + kolla_vars_file)
        
    if (os.path.exists (global_file)):
        config_vars.append("@" + global_file)

    if (os.path.exists (pass_file)):
        config_vars.append("@" + pass_file)

    if (os.path.exists (rabbit_vars)) :
        config_vars.append("@" + rabbit_vars)

    if not playbook:
        print("No playbook has been provided - exit the program :(")
        sys.exit(1)

    # Forge the desired call to Ansible:
    cmd = ["ansible-playbook"]
    cmd.extend(("-i", inventory))
    for config_var in config_vars:
        cmd.extend(("-e", config_var))
    if tags:
        cmd.extend(("--tags", tags))
    cmd.append(playbook)

    # Call ansible and raise exception if problems occured
    if run(cmd).returncode:
        raise Exception("Something wrong happened during the deployment.")

