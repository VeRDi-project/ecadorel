---
title: Objectives of the two months
author: Helene Coullon & Emile Cadorel
patat:
    incrementalLists: true
...

## Overview of objectives

1. Understanding Ansible

	* being able to execute basic deployments
	* being able to launch a Kolla-Ansible deployment from reproducible elements

2. Understanding Concerto/Madeus

	* being able to write a small example (Apache/MySQL deployment)
	* being able to launch the OpenStack deployment

3. Writing a first prototype constituted of

    * inputs:

        - Yaml input file containing dependencies
        - Ansible playbook, roles etc.
    
    * output: the Concerto/Madeus assembly and sub-playbooks ready to run

4. Writing the Grid'5000 template to run efficiency experiments

    * existing experiments can be reused (EnosLib is used)

5. Experiments

    * OpenStack
    * Other playbooks such as kubernetes cluster deployment

5. Thinking about inference of some dependencies for smarter tool

## Git repositories

- Concerto/Madeus:

    * [https://gitlab.inria.fr/VeRDi-project/concerto](https://gitlab.inria.fr/VeRDi-project/concerto)

        - ```./concerto/madeus```

- OpenStack Concerto/Madeus:

    * [https://gitlab.inria.fr/VeRDi-project/madeus-openstack](https://gitlab.inria.fr/VeRDi-project/madeus-openstack)

- Experiments on OpenStack

    * [https://gitlab.inria.fr/VeRDi-project/madeus-openstack-benchmarks](https://gitlab.inria.fr/VeRDi-project/madeus-openstack-benchmarks)

        - Kolla-ansible playbooks may also be here

- Reproducibility supposed to be

    * [https://gitlab.inria.fr/VeRDi-project/madeus-journal](https://gitlab.inria.fr/VeRDi-project/madeus-journal)

        - in ```org```

## Concerto/Madeus overview

Control component - instantiation

```python
from concerto.madeus.all import (MadeusComponent, DepType)

class Nova(MadeusComponent):
```

Methods to write:

- ```__init__```
- ```def create(self):```

## Concerto/Madeus overview

Control component - places

Inside ```create```

```python
class Nova(MadeusComponent):

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'ready',
            'restarted',
            'deployed'
        ]

        self.initial_place = 'initiated'
```

- simply a unique ```string``` identifier
- the initial place is also given

## Concerto/Madeus overview

Control component - transitions

Still inside ```create```

```python
self.transitions = {
    'register': ('initiated', 'deployed', self.register),
    'pull': ('initiated', 'pulled', self.pull),
    'config': ('initiated', 'pulled', self.config),
    'create_db': ('initiated', 'pulled', self.create_db),
    'upgrade_db': ('pulled', 'ready', self.upgrade_db),
    'upgrade_api_db': ('pulled', 'ready', self.upgrade_api_db),
    'restart': ('ready', 'restarted', self.restart),
    'cell_setup': ('restarted', 'deployed', self.cell_setup),
}
```

- from a source place to a destination place
- associated to a function = action

## Concerto/Madeus overview

Control component - actions

```python
def pull(self):
    host, ip = get_host_name_and_ip()
    logging.debug("host: %s - ip: %s : Pulling Nova start", host, ip)
    return run_ansible(self.playbook, self.inventory, ["action=pull"], self.vars_dir)

```

## Concerto/Madeus overview

Control component - ports

Still inside ```create```

```python
self.dependencies = {
    'mariadb': (DepType.USE, ['create_db']),
    'keystone': (DepType.USE, ['register']),
    'nova': (DepType.PROVIDE, ['deployed'])
}
```

- a ```String``` identifier
- two types: ```DepType.USE``` and ```DepType.PROVIDE``` 
- give the set of transitions for a ```DepType.USE```
- give the set of places for a ```DepType.PROVIDE```

## Concerto/Madeus overview

Assembly

Import object MadeusAssembly and components

```python
from concerto.madeus.all import MadeusAssembly
from assemblies.assembly_madeus.neutron import Neutron
from assemblies.assembly_madeus.nova import Nova
```

```python
class MMadeusAssembly(MadeusAssembly):
```

Methods to write:

- ```__init__```
- ```def create(self):```

## Concerto/Madeus overview

Assembly - instantiations and connections

```python
self.components = {
    "neutron": Neutron(...), #parameters of __init__
    "nova": Nova() #parameters of __init__
}
```

```python
self.dependencies = [
    ("mariadb", "mariadb", "nova", "mariadb"),
    ("keystone", "keystone", "neutron", "keystone"),
    ("keystone", "keystone", "nova", "keystone")
]
```

- (component name 1, port name 1, component name 2, port name 2)
- represents both data and service dependencies
- launch the assembly ```python assembly.py```

## Ansible -> Concerto/Madeus

- @Ansible playbook

    * @Ansible roles order give a strict order not enough for Madeus

- @Ansible Role = @Madeus Component

    * @Ansible Role `main.yaml` -> split inside transitions/actions
    * @Ansible Task -> @Madeus inside sub-playbooks inside actions
    * @Ansible Handlers -> unmodified, handled by Ansible
    * @Ansible templates Jinja2 -> unmodified, handled by Ansible

        - may be used for inference of data dependencies

## Example of possible Yaml file

- role name
- for each role list of dependencies :

    * data dependencies
    * service dependencies (default: last place of the component if no task specified)

- semantics _task X of role R1 waits the end of task Y of role R2_ 
	* _R2_ may be equal to _R1_ for internal dependencies

```yaml
---
- name: role_name
    tasks:
        - task_name1
        waits:
            - role-name1 task-name
            - role-name2 task-name
            - role-name tasks-name
        - task-name2
        waits:
            - ...
```

## Difficulties

- Split the playbook in sub-playbooks

    * the result should not be **1 task = 1 action** (unadapted granularity)
    * tasks must stay accumulated in sub-playbooks
        
        - my intuition: between two dependencies merge tasks in the same sub-playbook

- Split tasks?

    * in the OpenStack example, I think that some tasks of `Nova` has also been split
    * almost impossible to catch automatically

- Smarter tool?

    * From Jinja2 templates extract data dependencies
        - intuition : if there is a data dependency there is probably a service dependency also, maybe some default service dependencies could be determined
    * From handlers? anything?

## Miscellaneous

- Gathering facts (constant cost when lauching Ansible)

    * See file ```ansible.cfg``` in the OpenStack experiments I think

- What about Ansible handlers?

    * currently left outside Madeus/Concerto and handled by Ansible itself

- What about Terraform+Ansible -> Terraform+Madeus

- What about mining Ansible Galaxy to get more examples?

    * [https://galaxy.ansible.com/](https://galaxy.ansible.com/)

- What about learning from other examples usual dependencies and patterns?