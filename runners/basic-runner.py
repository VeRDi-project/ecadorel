#!/usr/bin/env python3

import logging

import sys
import click


from utils.constants import ANSIBLE_DIR

from assemblies.mmadeusassembly import MMadeusAssembly

@click.command(help="Launch the assembly")

@click.option("-g", "--gantt",
              is_flag=True,
              help="Give a name for generating Gantt graphs.")
@click.option("-gn", "--gantt_name",
              default="server",
              help="Give a name for generating Gantt graphs.")
@click.option("-i", "--ansible_inventory",
              required=True,
              type=click.Path(exists=True),
              help="Path to the Ansible inventory to use.")
@click.option("-v", "--vars_dir",
              type=click.Path(dir_okay=True),
              help="Path to the directory containing globals.yml and password.yml.")
def main(gantt, gantt_name, ansible_inventory, vars_dir):

    logging.basicConfig(filename='concerto.log',
                        format='%(threadName)s - %(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(message)s',
                        level=logging.DEBUG)

    the_log = logging.getLogger("testing")

    logging.debug("Testing")

    assembly = MMadeusAssembly ("./ansible", ansible_inventory, vars_dir)
    if gantt: 
        assembly.set_record_gantt(True)
        
    assembly.set_verbosity(1)
    # Set a timeout regarding our deployment
    assembly.run ()

    if not assembly.get_error_reports():# and not assembly.get_error_reports():
        print("Deployement successful")
        assembly.terminate()

        # The following is used to generate Gantt charts
        if gantt:
            file_name = "results_" + gantt_name
            gc = assembly.get_gantt_record().get_gantt_chart()
            gc.export_json(file_name + ".json")

            sys.exit(0)

    if assembly.get_error_reports():
        print("Deployement failed")
        print (assembly.get_error_reports ())
        assembly.kill()


    sys.exit(1)


if __name__ == '__main__':
    main()

