#!/usr/bin/env python3

import logging

import sys
import click
from subprocess import run

from utils.constants import ANSIBLE_DIR

from assemblies.assembly_m_ansible.assembly import MAnsibleAssembly
from assemblies.assembly_m_aeolus.assembly import MAeolusAssembly
from assemblies.assembly_madeus.assembly import MMadeusAssembly
from assemblies.assembly_m_sequential.assembly import MSequentialAssembly

assemblies = ['m_ansible', 'm_aeolus', 'madeus', 'm_sequential']



@click.command(help="Launch the assembly")
@click.option("-a", "--assembly_type",
              required=True,
              type=click.Choice(assemblies),
              help="Select the assembly type.")
@click.option("-d", "--ansible_dir",
              type=click.Path(dir_okay=True),
              help="Path to the directory containing Ansible playbooks.")
@click.option("-g", "--gantt",
              is_flag=True,
              help="Give a name for generating Gantt graphs.")
@click.option("-gn", "--gantt_name",
              default="server",
              help="Give a name for generating Gantt graphs.")
@click.option("-i", "--ansible_inventory",
              required=True,
              type=click.Path(exists=True),
              help="Path to the Ansible inventory to use.")
@click.option("-t", "--timeout",
              type=int,
              default = 2000,
              help="Path to the Ansible inventory to use.")
@click.option("-v", "--vars_dir",
              type=click.Path(dir_okay=True),
              help="Path to the directory containing globals.yml and password.yml.")
def main(assembly_type, ansible_dir, gantt, gantt_name, ansible_inventory,
        timeout, vars_dir):

    logging.basicConfig(filename='concerto.log',
                        format='%(threadName)s - %(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(message)s',
                        level=logging.DEBUG)

    the_log = logging.getLogger("testing")

    logging.debug("Testing")
    assembly = None
    ansible_dir = ansible_dir or ANSIBLE_DIR
    if assembly_type == "m_ansible":
        assembly = MAnsibleAssembly(ansible_dir, ansible_inventory, vars_dir)
    elif assembly_type == "m_aeolus":
        assembly = MAeolusAssembly(ansible_dir, ansible_inventory, vars_dir)
    elif assembly_type == "madeus":
        assembly = MMadeusAssembly(ansible_dir, ansible_inventory, vars_dir)
    elif assembly_type == "m_sequential":
        assembly = MSequentialAssembly(ansible_dir, ansible_inventory, vars_dir)
    assembly.set_verbosity(1)
    if gantt:
        assembly.set_record_gantt(True)

    # Set a timeout regarding our deployment
    finished, debug_info, _ = assembly.run_timeout(timeout)

    if finished and not assembly.get_error_reports():# and not assembly.get_error_reports():
        print("Deployement successful")
        assembly.terminate()

        # The following is used to generate Gantt charts
        if gantt:
            file_name = "results_" + gantt_name
            gc = assembly.get_gantt_record().get_gantt_chart()
            gc.export_json(file_name + ".json")

            sys.exit(0)

    if assembly.get_error_reports():
        print("Deployement failed")
        assembly.kill()

    if not finished:
        print("Deployement failed")
        print("Error! Timeout on deploy! Debug info:")
        print(debug_info)
        assembly.kill()
        # Sometimes ansible-playbook is stuck, here's an ugly workaround:
        run(["pkill -f ansible-playbook"])

    sys.exit(1)


if __name__ == '__main__':
    main()

